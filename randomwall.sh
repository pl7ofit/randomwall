#!/bin/bash

# LICENSE: GNU General Public License version 3
# https://www.gnu.org/licenses/gpl-3.0.ru.html

set -e

ua="Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.357 Safari/537.36"
genre=nature
#3840x2400
#3840x2160
#2560x1600
#2560x1440
#2560x1080
#2560x1024
#2048x1152
#1920x1200
#1920x1080
#1680x1050
#1600x900
#1440x900
#1280x800
#1280x720
q=3840x2160
site="https://wallpaperscraft.ru/catalog/$genre/$q"
path=$(xdg-user-dir PICTURES)/Wallpapers/$genre/$q
if [ ! -d $path ]
then
	mkdir -p $path
fi
cd $path


while [ -z "$lastpage" ]
do
	echo "Last page is empty, try again..."
	lastpage=$(wget -q -O- --user-agent="$ua" $site/ | grep "Последняя"| cut -d'"' -f4|cut -d'/' -f5| sed 's/[^0-9]*//g')
	sleep 0.7
done


startpage=$(seq 0 $lastpage | sort -R | head -n 1)
#startpage=0

for page in $(seq $startpage $lastpage)
do
	page=page$page
	for pic  in $(wget -q -O- --user-agent="$ua" $site/$page/| grep "alt="|cut -d'"' -f4| sed "s/300x168/$q/g"| grep https )
	do
		echo $pic
		file="$path/$(echo $pic| grep -oP '[a-zA-Z0-9\_]{5,80}\.[a-z]{3,4}$')"
		wget --user-agent="$ua" -nc $pic|zenity --progress  --auto-close --pulsate
		echo $file
		echo write /org/gnome/desktop/background/picture-uri  "'"$file"'"
		dconf write /org/gnome/desktop/background/picture-uri  "'"$file"'"
		zenity --question --text "Next?"
		if [[ $? == 1 ]]
		then
			exit 0
		fi

	done
done

exit 0
